nose==1.3.4
expects==0.6.2
coverage==3.7.1
flake8==2.2.5
mccabe==0.2.1
mock==1.0.1
